// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCjDXZwiV2DZsy1wC9RZfAno_wamvtJclk",
    authDomain: "fir-task-d0bd3.firebaseapp.com",
    databaseURL: "https://fir-task-d0bd3-default-rtdb.firebaseio.com",
    projectId: "fir-task-d0bd3",
    storageBucket: "fir-task-d0bd3.appspot.com",
    messagingSenderId: "752690361455",
    appId: "1:752690361455:web:ee9fcd5b5d294bd4b5099a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
