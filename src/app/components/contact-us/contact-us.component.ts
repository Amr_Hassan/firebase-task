import { Component, OnInit } from '@angular/core';
import {FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Contcat } from '../../interfaces/contcat'
import { FirbasedbService } from '../../services/firbasedb.service'



@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  // emailFormControl = new FormControl('', [
  //   Validators.required,
  //   Validators.email,
  // ]);

  

  submitted:boolean = false
  faildsubmitted:boolean = false
  newContcat:Contcat = {
    first_name : '' , 
    last_name:'',
    age:0,
    email: '',
    nationality: '',
  };


  constructor(private fbservice:FirbasedbService , private route:Router) { }

  ngOnInit(): void {
    
  }

  saveContacts(event , first:string , last:string , email:string,age:number , nat:string ): void{

    

    if (first.length > 0 && last.length > 0 && email.length > 0 && age > 0 && nat.length > 0){
      const postTime = new Date()

      event.preventDefault()
      this.newContcat.first_name = first
      this.newContcat.last_name = last
      this.newContcat.email = email
      this.newContcat.age = age
      this.newContcat.nationality = nat
      this.newContcat.time = `${postTime.getFullYear()} ${postTime.getMonth()+1} ${postTime.getDate()}`
  
      this.fbservice.create(this.newContcat).then(() => {
        this.submitted = true
          // this.route.navigate(['/admin'])
        });
    }else{
      this.faildsubmitted = true
    }

   
  }
}
