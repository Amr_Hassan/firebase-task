import { Component, OnInit } from '@angular/core';
import { Contcat } from '../../interfaces/contcat'
import { FirbasedbService } from '../../services/firbasedb.service'
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  newContcat:Contcat = {
    first_name : '' , 
    last_name:'',
    age:0,
    email: '',
    nationality: '',
    time:''
  };

  displayedColumns: string[] = ['first_name', 'last_name', 'age', 'email' , 'nationality' , 'date'];
  dataSource:Array<Contcat>;
  constructor(private fbservice:FirbasedbService) { }

  ngOnInit(): void {
    this.fbservice.getContcats().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        )
      )
    ).subscribe(data => {    
      this.dataSource = data;
    });
  }
}