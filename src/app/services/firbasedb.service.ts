import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database'
import { Contcat } from '../interfaces/contcat'


@Injectable({
  providedIn: 'root'
})
export class FirbasedbService {
  private dbPath = '/contacts';
  contactsRef: AngularFireList<Contcat>;

  constructor( private db:AngularFireDatabase) { 
    this.contactsRef = db.list(this.dbPath);
  }
  
  getContcats():AngularFireList<Contcat>{
    return this.contactsRef;
  }

  create(newContcat: Contcat): any {
    return this.contactsRef.push(newContcat);
  }

}



