import { TestBed } from '@angular/core/testing';

import { FirbasedbService } from './firbasedb.service';

describe('FirbasedbService', () => {
  let service: FirbasedbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirbasedbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
