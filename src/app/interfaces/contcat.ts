export interface Contcat {
    id?:number,
    first_name:string,
    last_name:string,
    age:number,
    email: string,
    nationality: string,
    time?:string
}
